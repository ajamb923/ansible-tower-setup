// Variables

variable "ami" {
    default = "ami-06640050dc3f556bb"
}

variable "instance_type" {
    default = "t3.large"
}

#variable "security_groups" {}

variable "keyname" {
    default = "NOVA_KP"
}

variable "ebs_volume_size" {
    default = 30
}

variable "ebs_volume_type" {
    default = "gp2"
}