// EC2 instance

resource "aws_instance" "this-instance" {
  ami = var.ami
  instance_type = var.instance_type
  user_data = file("userdata.sh")
  security_groups = [aws_security_group.this-sg.name]
  key_name = var.keyname

  root_block_device {
    volume_size = var.ebs_volume_size
    volume_type = var.ebs_volume_type
  }

  tags = {
    Name = "Tower-Instance"
    project = "Ansible_Tower"
  }

}


// Security Group
resource "aws_security_group" "this-sg" {
  name = "this-tower-sg"
  description = "Security group for Ansible Tower"
  
  ingress {
    description = "SSH"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTPS"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "this-Tower-SG"
  }
}