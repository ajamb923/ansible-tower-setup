#!/bin/bash
sudo su
sudo dnf install ansible -y
sudo yum install wget -y
sudo curl "https://releases.ansible.com/ansible-tower/setup/ansible-tower-setup-latest.tar.gz" --output /home/ec2-user/ansible-tower-setup-latest.tar.gz
sudo tar -xzvf /home/ec2-user/ansible-tower-setup-latest.tar.gz -C /home/ec2-user
sudo sed -i "s/admin_password=''/admin_password='ansible'/g" /home/ec2-user/ansible-tower-setup-3.8.6-2/inventory
sudo sed -i "s/pg_password=''/pg_password='ansible'/g" /home/ec2-user/ansible-tower-setup-3.8.6-2/inventory
sudo /home/ec2-user/ansible-tower-setup-3.8.6-2/setup.sh .


# #install docker:
# sudo yum install docker -y
# sudo systemctl start docker 
# sudo systemctl enable docker 